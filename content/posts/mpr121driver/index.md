+++
title = "Mpr121 capacitive touch sensor driver"
description = "Discussing the implementation of the Mpr121-hal crate and its usage."
date = 2022-11-04

[taxonomies]
tags = ["rust", "embedded", "sensor"]
categories = ["Embedded"]

+++

## Writing a driver (again)


The last driver for the FS1027-Dg flow sensor was motivated by my MIDI-Sax build. My first prototype used this sensor and a couple of buttons to assemble the sax. The result works surprisingly well. But normal buttons tampered with the playing experience since it is not always easy to press such small buttons. 
My next approach was using MX-Switches (known from mechanical keyboards) to get a better *key press* experience. However the travel was way too long in practice. Which brings me to touch based keys. 


Researching the topic brought me to Adafruit's [Mpr121](https://www.adafruit.com/product/1982) capacitive touch sensor. Its exactly what I need (I²C based and >= 10 channels). So I decided to buy the board. 


While waiting for the board I had searched for Rust HAL integration. I found two related crates [mpr121](https://crates.io/crates/mpr121) and [adafruit-mpr121](https://crates.io/crates/adafruit-mpr121). Both didn't fit my needs. The first one targets embedded programming, but doesn't seem to be fully implemented at the moment. The second one targets RaspberryPi + Linux. It doesn't use `embedded-hal` but the Linux I²C driver.

So I decided to write my own `embedded-hal` implementation.


While implementing the driver I had two main references, the [C++ implementation](https://github.com/zaczkows/adafruit-mpr121) from Adafruit, and the [data sheet](https://www.nxp.com/docs/en/data-sheet/MPR121.pdf). My goal was to mirror the C++ implementation as close as possible, since this is probably the way most people are used to interact with the board. After all its sold mostly by Adafruit. I also wanted to introduce the rusty way of handling errors and data. The result is a similar API with better error reporting and some convenience functions. 

## Usage


The crate is hosted on [crates.io](https://crates.io/crates/mpr121-hal) and comes with [documentation](https://docs.rs/mpr121-hal/latest/mpr121_hal/). However, a quick start would be to setup a I²C connection between the sensor and your dev-board like this: 

![Connecting MPR121 module to esp32](Mpr121Connections.jpg)


In code you have to create a valid I²C instance using `embedded-hal` and create a `mpr121` instance using that bus. This look similar to the pseudocode below.

```rust

use your_board_hal::i2c::I2C;
use mpr121-hal::Mpr121;

fn main(){

    //... gpio setup code for your device

    let i2c = I2C::new(gpio.sdl, gpio.sdc);
    let mut mpr = Mpr121::new_default(i2c);
    
    loop{
        for channel in 0..12{
            println("[{}]: {}", channel, mpr.get_sensor_touch(channel));
        }
    }
    
    //if you need back the I²C bus, or the gpio pins, use free
    let i2c = mpr.free();
}

```

### Addresses

The board can be configured to respond to different addresses on the I²C line. This is helpfull if another device already uses the address on the same line, or if you use multiple mpr121 boards on the same line.

By default the address is `0x5a`. This is the address used by `Mpr121::new_default` as well. If you connect the `addr` pin of the mpr121 board to `Vdd`, `Sdc` or `Sdl` you change the address the board listens on. To set the address use `Mpr121::new()` where the address mirrors your physical `addr` pin connection.
