+++
title = ""
description = ""
+++

Siebencorgie's blog and site for nerdy stuff. This is mostly about graphics and audio programming as well as occasional nonsense. Have a look at my posts, or the gallery of projects I did myself or was involved in.
