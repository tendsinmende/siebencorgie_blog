+++
title = "Jakar-Engine"
description = "Vulkan based graphics engine"
date = 2022-10-17
updated = 2022-10-17

[taxonomies]
tags = ["rust", "vulkan", "graphics"]

+++


# Jakar engine

- [🔗 Git](https://gitlab.com/tendsinmende/jakar-engine/-/tree/master/)
- [🎥 First Video](https://youtu.be/Fzg4XTbOIAY)(Was called *Ori Engine* at that time.)
- [🎥 Last Video](https://www.youtube.com/watch?v=P2jrkP6y8Lo)


This was my first larger *engine* project. It centers around traditional polygon models and uses a simple PBR pipeline for rendering. I wrote two companion crates at the time. [Jakar-Tree](https://gitlab.com/Siebencorgie/jakar-tree.git): a scene tree implementation inspired by Godot's scene tree and [jakar-threadpool](https://gitlab.com/Siebencorgie/jakar-threadpool.git), a simple thread pool for dispatching async work.


I used the project to try out more sophisticated shaders and techniques like a bloom implementation, cascaded shadow mapping, and later ray traced shadows using a custom build BVH and traversal compute shader. 

A funny detail is my clustered light implementation. I could not get the `light <-> cluster` assignment right at the time, which is why the lights are assigned to a world-space cluster instead. Basically a *light voxel volume*.

The whole project used [Vulkano](https://github.com/vulkano-rs/vulkano) which was a nice gentle introduction into Vulkan/DX12 ways of working with your graphics card. For the next project I decided to use *raw* Ash/Vulkan. Mostly to get better control over memory management and easier access to extensions.


The following list of features were at some point implemented, but some were removed again while the project developed:

- PBR shading (no GI though)
- (World space) Clustered Forward Rendering
- Deferred Rendering
- Physics based lighting and camera (using lux/lumen)
- Dynamic Eye adaption (using scene luminosity feedback and luminosity histogram)
- Bloom 
- Ray traced shadows (using custom BVH builder and traversal shader)



A nice representation of the rendering quality is the last video regarding it:

{{ youtube(id="P2jrkP6y8Lo") }}
