+++
title = "Tikan Renderer"
description = "Vulkan based voxel renderer"
date = 2022-10-17
updated = 2022-10-17

[taxonomies]
tags = ["rust", "vulkan", "graphics", "voxel"]

+++


# Tikan engine

- [🔗 Git](https://gitlab.com/tendsinmende/tikan)
- [🎥 Video](https://www.youtube.com/watch?v=98XdA3BpWZU)


Tikan was the next project after [Jakar](/gallery/jakar-engine). It is my take on high resolution voxel rendering. Since it does not do much more than rendering, the term *engine* doesn't really fit here. It is inspired by the [Atomontage engine](https://www.youtube.com/c/AtomontageInc) engine and [John Lin's](https://www.youtube.com/channel/UCM2RhfMLoLqG24e_DYgTQeA) voxel experiments at the time.

Before starting the project, I created my first wrapper around Ash/Vulkan called [Marp](https://gitlab.com/tendsinmende/marp). Utilizing that, I created my first try on a widget toolkit called [Widkan](https://gitlab.com/tendsinmende/widkan). Both are used in Tikan.

While working on Tikan's voxelizer for GLTF2 objects, I briefly worked on [Voxer](https://gitlab.com/tendsinmende/voxer). The goal was to unify different representations of voxels, similar to the [image](https://crates.io/crates/image) crate, but in 3D. In the end such a unification proved to be not as easy as I thought, so it only became the CPU side container for voxels in Tikan.

The project had two main goals.

1. See how hard it is to render high resolution voxel objects efficiently
2. See if we can calculate GI efficiently from such a voxel scene

The first point is more or less a problem of optimising memory usage and *primary ray* tracing. In the end I settled for a sparse voxel octree since they are easy to build and maintain, while also being easy to trace.

The second point is a little more involved. I tried multiple approaches of gathering GI. The final version is based on the [voxel cone](https://research.nvidia.com/sites/default/files/publications/GIVoxels-pg2011-authors.pdf) idea. My hope was, that both shadows and light emitters could be solved by the same GI gathering pass. The advantage would be that you don't have to implement multiple emitter types, but just mark a voxel as *emitting light*. In the end, the voxel cones worked well for low frequency effects like GI, but were not suitable to solve all incoming direct light. The main problem was the wide cone angle needed to get a fast enough calculation. It leads to those block-ish light areas seen in the [video](https://www.youtube.com/watch?v=98XdA3BpWZU) below the light emitting spheres.

A nice property of this approach is the constant evaluation cost. Since there are no dedicated emitter objects, the rendering cost does not scale with the number of emitters in the scene at all. Adding light is therefore basically free.


{{ youtube(id="98XdA3BpWZU") }}
