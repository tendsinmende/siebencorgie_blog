+++
title = "Sax32"
description = "Saxophone like MIDI controller"
date = 2022-05-29
updated = 2022-09-09
draft = true

[taxonomies]
tags = ["rust", "midi", "audio", "embedded"]

+++

# 🎷 Sax32 🎷

The sax32 is a concept Saxophone like MIDI controller. The code and 3D-CAD files are on [GitLab](https://gitlab.com/tendsinmende/sax32). But be warned, as specially the CAD stuff is messy 😬. The current working version is at [this](228fa0928bdad0d1a931b3b17d96248048a7c470) commit. But its far from being *done*. I plan on making a more sophisticated version in the future.

The Project consists of three main parts:

1. The hardware (CAD files can are in the `models` directory)
2. Hardware's firmware (`crates/sax_firmware`)
3. Companion App (`crates/serial_midi_client`)

The firmware is build for a `Esp32-C3` and emits a midi like protocol on the SerialUSB connection. Have a look at the firmware and the `crates/sax_protocol` crate for more information.
The companion app reads those serial messages and passes them into a [JACK](https://jackaudio.org/) port that can then be hooked up either to a DAW or a synthesizer directly.

The companion app has a UI that allows changing the flow sensors mapping, as well as changing the base note of the Sax. 


TODO: video
TODO galery: 

### Hardware

#### Parts and printing

What you need to build the sax is:
1. FS1027-DG Flow Sensor,
1.1 Flow sensor plug and cable (The JST plug and 4 jumper cables)
2. Esp32-c3
3. Breadboard
4. 7 buttons
5. 3D printer and some filament

The 3d Printing parts are the `.stl` files in the `models` directory. The main parts are the clip, the two body parts and the tubes. The tubes should be made from TPU. The rest can be anything.

#### Wireing

TODO add a wiring scheme

### Softawre

The firmware can be flashed on the controller by connecting it via USB and executing the following within the `creates/sax_firmware` directory:
```bash
cargo espflash --release /dev/ttyUSB0
```

assuming that `USB0` is the controller's USB port.

This should start the controller as well. The serial messages can be monitored on the USB port, for instance via
```
espmonitor /dev/ttyUSB0
```

The client software (that opens the MIDI JACK port) can be executed in the `crates/serial_midi_client` directory via:
```bash
cargo run --release
```



## Closing

That's it for this prototype*ish* MIDI controller. I had a lot of fun building it and have some ideas for a second version. For instance the buttons could be cherry keys known from mechanical keyboards. They should provide a nice tactile feedback.
Another idea is to use touch-based resistors to select a note by *sliding* a finger along the length of the sax's body. 
Another major improvement would be to eliminate the USB cable and use the WIFI sender instead to communicate with the client.

If you have other ideas let me know through one of the channels listed on the [homepage](/)!
