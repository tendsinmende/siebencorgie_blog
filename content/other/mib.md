+++
title = "Most important resources"
description = "A list of blog posts and papers I find interesting and worthy to have a look at."
date = 2022-03-11
updated = 2022-03-11

[taxonomies]
tags = ["blog", "link", "pdf"]
+++

## Blog
Blogs that are interesting and worth a look. 

### Senvas
The [site](https://fragmental.art/senvas/) covers music, graph based parallelization (via [redgrapes](https://github.com/ComputationalRadiationPhysics/redGrapes)) and researched for (typed?) structure editing ([nested](https://git.exobiont.de/senvas/nested)).

If you are into psychedelic art, also have a look at its top-level [fragmental.art](https://fragmental.art).

### Fasterthanlime

[Amos](https://fasterthanli.me/) produces high quality deep dives about Rust (and low level programming) related topics. They mostly start understandable and become increasingly complex.

### Iquilezles

[Inigo Quilez](https://iquilezles.org/) has a lot of posts online related to math, graphics and, most importantly signed-distance-fields. I believe his [2D](https://iquilezles.org/www/articles/distfunctions2d/distfunctions2d.htm) and [3D](https://iquilezles.org/www/articles/distfunctions/distfunctions.htm) signed distance function collections has been the starting point for many people getting into SDFs including me.

### Jendrik Illner

[Jendrik](https://www.jendrikillner.com/post/) compiles a list of the most important graphics programming related happenings each week.

### The blog at the bottom of the sea

[The Blog](https://blog.demofox.org/) covers math, audio and graphics programming related topics. As specially when getting started in audio programming this is a good location to get a basic understanding of filters, time and frequency domain.

## Posts
🔨 Work in progress 🔨


## Papers
🔨 Work in progress 🔨


## Libraries and crates

I'm mostly using Rust at the moment, so these are mostly crates (Rust-Ecosystem word for library).

### Rust
- [Hotwatch](https://crates.io/crates/hotwatch) allows you to watch files in your file system and execute a function if something changes. Not setup needed!
- [Puffin](https://crates.io/crates/puffin) easy profiling of specific function. There is a [web based interface](https://github.com/EmbarkStudios/puffin/tree/main/puffin_viewer) that allows reading the profiles in realtime.
- [ash](https://crates.io/crates/ash) Low-Level Vulkan bindings. 
- [winit](https://crates.io/crates/winit) Rust based window creation. Also provides raw window handles/pointer that can be used with vulkan
- [rust-gpu](https://github.com/EmbarkStudios/rust-gpu) Rust -> SpirV compiler/utilities. Allows you to write shaders using rust that are compiled to SpirV. Used by my Algae JIT-Compiler.
