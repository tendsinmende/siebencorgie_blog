+++
title = "Rust-Gpu cheat sheet"
date = 2022-10-03
updated = 2022-10-03

[taxonomies]
tags = ["rust", "compiler", "spirv", "rust-gpu"]
+++

Working with Rust-Gpu sometimes is unfamiliar if you are used to GLSL/HLSL. Below are some fixes/hints for problems I encountered.

Each bug description contains the date the problem was encountered. Problems, as specially bugs and missing features a probably fixed at some point. The workaround/fix might have become invalid in the meantime.

## Errors


> 2022-10-03: missing `panic_handler`

Happens if you do not include `spirv_std` in your shader crate. It provides the handler.

---

> 2022-10-03: can't find/use **std**

Happens if you have miss the `#![no_std]` attribute in your crate **or** include a crate that does not include it.

---

> 2022-10-03: `OpVariable, <id> '19[%some_variable]', is attempting to create memory for an illegal type, OpTypeRuntimeArray`

Happens [if you use a runtime array](https://github.com/EmbarkStudios/rust-gpu/issues/879) (slice) without setting the `RuntimeArray` Capability on the `SpirvBuilder` while compiling.

---

> 2023-01-15: `failed to resolve: use of undeclared crate or module `spirv_std``

Happens if you are using the `Image` macro, but don't have a `spirv_std` crate in scope. To fix, just put `use path::to::some::spirv_std` somewhere in the file.

---


## How to *x*

> 2022-10-03: Include unbond array of images. Basically `layout(set = 1, binding = 0) uniform image2D all_images[];` in GLSL.

Use `RuntimeArray<T>`, note that `T` must be sized. Like this:

```rust
#[spirv(descriptor_set = 1, binding = 0)] all_images: &RuntimeArray<Image!(2D, type=f32, sampled=false)>
```

**Doesn't work with buffers yet!**
**Update: 2023-01-15**: There is now some highly nightly way to include [StorageBufferArrays](https://github.com/SiebenCorgie/rust-gpu-buffer-indexing/blob/c58ea2401f64fe0c61acc30660647d0f56a7a4bd/crates/shader/src/lib.rs#L19).

---

> 2022-10-03: Borrow a part of a slice into a function like this: `foo(&my_array[4..10])`

This currently does [not work](https://github.com/EmbarkStudios/rust-gpu/issues/752). What you can do however is borrow the whole slice and add additional arguments for the range. For instance like this:

```rust
fn foo(a: &[u32], start: u32, end: u32){
    for i in start..end{
        //...
    }
}
```
